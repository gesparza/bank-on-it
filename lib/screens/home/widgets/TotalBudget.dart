import 'package:flutter/material.dart';

class TotalBudgetDisplay extends StatelessWidget {
  TotalBudgetDisplay({Key key, this.budgetText}) : super(key: key);

  final String budgetText;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('\$$budgetText',
            style: TextStyle(fontSize: 60.0, fontWeight: FontWeight.bold)),
        Text('Dollars left in your budget'),
      ],
    );
  }
}
