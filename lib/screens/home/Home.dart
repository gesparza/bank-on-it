import 'package:flutter/material.dart';

import '../../data/database/Database.dart';
import '../../data/models/Budget.dart';

import 'widgets/TotalBudget.dart';
import '../../widgets/TextFormSubmitNumber.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var db = DBHelper();
  final inputController = TextEditingController();
  final focusController = FocusNode();
  Budget myBudget;

  Future<Budget> _loadBudgetData() async {
    db.addBudget(Budget(14, 0, 1500.0));
    myBudget = await db.getBudget();
    return myBudget;
  }

  void _updateBudget(costText) async {
    setState(() {
      myBudget.total -= double.parse(costText);
      db.makeTransaction(myBudget);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FutureBuilder(
                future: _loadBudgetData(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting)
                    return Text('Waiting for data');
                  else if (snapshot.hasData) {
                    return TotalBudgetDisplay(
                        budgetText: snapshot.data.total.toStringAsFixed(2));
                  } else
                    return Text('didnt work');
                }),
            Container(
                margin: EdgeInsets.all(32.0),
                child: SubmitSpent(
                    input: inputController, onSubmit: _updateBudget)),
          ],
        ),
      ),
    );
  }
}
