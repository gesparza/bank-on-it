class Budget {
  Budget(this.dayLength, this.transactionCount, this.total);

  int id;
  int dayLength;
  int transactionCount;
  double total;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'day_length': dayLength.toString(),
      'trans_count': transactionCount.toString(),
      'total': total.toString()
    };
    if (id != null) {
      map['id'] = id;
    }
    return map;
  }

  Budget.fromMap(Map<String, dynamic> map) {
    id = int.parse(map['id']);
    dayLength = int.parse(map['day_length']);
    transactionCount = int.parse(map['trans_count']);
    total = double.parse(map['total']);
  }

 void setId(int id) {
    this.id = id;
  }
}
