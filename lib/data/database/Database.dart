import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

import '../models/Budget.dart';

class DBHelper {
  static const BUDGET_TABLE_NAME = "budget";
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  //Creating a database with name test.dn in your directory
  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "bankonit.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  } // _initDb()

  // Creating a table name Employee with fields
  void _onCreate(Database db, int version) async {
    String command = 'CREATE TABLE ' +
        BUDGET_TABLE_NAME +
        ' (' +
        'id INTEGER PRIMARY KEY, ' +
        'day_length TEXT, ' +
        'trans_count TEXT, ' +
        'total TEXT' +
        ')';

    // When creating the db, create the table
    await db.execute(command);

    print("Created tables");
  } // _onCreate()

  // Retrieving employees from Employee Tables
  Future<Budget> getBudget() async {
    var dbClient = await db;
    List<Map> result = await dbClient.rawQuery('SELECT * FROM Budget');
    Budget myBudget = Budget(int.parse(result[0]["day_length"]),
        int.parse(result[0]["trans_count"]), double.parse(result[0]['total']));

    myBudget.setId(result[0]["id"]);
    print('Got budget');
    return myBudget;
  } // getBudget()

  void addBudget(Budget newBudget) async {
    final dbClient = await db;
    await dbClient.insert(BUDGET_TABLE_NAME, newBudget.toMap());
    print('Inserted new budget');
  }

  makeTransaction(Budget bud) async {
    final dbClient = await db;
    var res = await dbClient.update(BUDGET_TABLE_NAME, bud.toMap(),
        where: "id = ?", whereArgs: [bud.id]);
    return res;
  }
}
