import 'package:flutter/material.dart';

class SubmitSpent extends StatelessWidget {
  SubmitSpent({Key key, this.input, this.onSubmit}) : super(key: key);

  final input;
  final Function onSubmit;
  final focusController = FocusNode();

  Widget build(BuildContext context) {
    return TextFormField(
      textAlign: TextAlign.center,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      textInputAction: TextInputAction.done,
      onFieldSubmitted: (value) {
        onSubmit(value);
        input.clear();
      },
      controller: input,
      decoration: new InputDecoration(
        labelText: 'How much did you spend?',
        border: OutlineInputBorder(),
        contentPadding: const EdgeInsets.all(16.0),
      ),
    );
  }
}
